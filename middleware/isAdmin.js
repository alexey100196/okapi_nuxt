export default function({ store, redirect }) {
  if (store.state.auth.user.role === 'contributor') {
    return redirect('/dashboard');
  }
}
