export default function({ store, redirect }) {
  if (store.state.auth.user.role === 'superAdmin') {
    return redirect('/admin-super/dashboard');
  }
}
