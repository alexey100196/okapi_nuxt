(function(apiKey) {
  (function(p, e, n, d, o) {
    let v, w, x, y, z;
    o = p[d] = p[d] || {};
    o._q = [];
    // eslint-disable-next-line prefer-const
    v = ['initialize', 'identify', 'updateOptions', 'pageLoad', 'track'];
    for (w = 0, x = v.length; w < x; ++w)
      (function(m) {
        o[m] =
          o[m] ||
          function() {
            o._q[m === v[0] ? 'unshift' : 'push'](
              [m].concat([].slice.call(arguments, 0))
            );
          };
      })(v[w]);
    // eslint-disable-next-line prefer-const
    y = e.createElement(n);
    y.async = !0;
    y.src = 'https://cdn.pendo.io/agent/static/' + apiKey + '/pendo.js';
    // eslint-disable-next-line prefer-const
    z = e.getElementsByTagName(n)[0];
    z.parentNode.insertBefore(y, z);
  })(window, document, 'script', 'pendo');

  // Call this whenever information about your visitors becomes available
  // Please use Strings, Numbers, or Bools for value types.
  // eslint-disable-next-line no-undef
})('5559f569-68dc-434b-6244-f42ca019c5a3');
