import { v4 as uuid } from 'uuid';

export const actions = {
  async addUpdate({ commit }, payload) {
    try {
      // const { data } = await this.$axios.$post('url', payload)

      await commit('ADD_UPDATE', payload);
    } catch (error) {
      // console.error(error)
    }
    // return state.commit('setToken', 'asdasfhsdbasda231sad2signup')
  },
  async deleteUpdate({ commit, state }, payload) {
    try {
      const updateIndex = state.updates.findIndex(
        (item) => item.id === payload
      );
      // console.log(updateIndex)
      await commit('DELETE_UPDATE', updateIndex);
    } catch (e) {
      // console.error(e)
    }
  }
};

export const mutations = {
  ADD_UPDATE(state, update) {
    state.updates.push(update);
  },
  DELETE_UPDATE(state, updateIndex) {
    state.updates.splice(updateIndex, 1);
  }
};

export const state = () => ({
  updates: [
    {
      id: uuid(),
      status: 'On track',
      class: 'update-status update-status--ok',
      message:
        'We’ve got the stylesheet in place, working through the Dashboard screen next.',
      time: 'a day ago',
      user: {
        name: 'John Doe',
        image: '~assets/images/john.jpg'
      }
    },
    {
      id: uuid(),
      status: 'Off Track',
      class: 'update-status update-status--off',
      message:
        'We’re a little behind but big plans for next week to get back on track',
      time: 'a week ago',
      user: {
        name: 'John Doe',
        image: '~assets/images/john.jpg'
      }
    },
    {
      id: uuid(),
      status: 'At risk',
      class: 'update-status update-status--risk',
      message:
        'We’ve got the stylesheet in place, working through the Dashboard screen next.',
      time: 'a day ago',
      user: {
        name: 'John Doe',
        image: '~assets/images/john.jpg'
      }
    }
  ]
});
