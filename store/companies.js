export const actions = {
  async getCompanies({ commit }) {
    try {
      const { data } = await this.$axios.get('/companies/get/all');
      await commit('SET_COMPANIES', data);
    } catch ({ response }) {
      const msg = {
        status: response.data.statusCode,
        info: response.data.message,
        error: response.data.error
      };
      commit('notifications/ADD_SNACKBAR_MESSAGE', msg, { root: true });
    }
  }
};
export const mutations = {
  SET_COMPANIES(state, companies) {
    state.companies = companies;
  }
};

export const getters = {};

export const state = () => ({
  companies: []
});
