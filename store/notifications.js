import { v4 as uuid } from 'uuid';

export const mutations = {
  ADD_SNACKBAR_MESSAGE(state, msg) {
    const newMsg = {
      id: uuid(),
      status: msg.status,
      info: msg.info,
      error: msg.error
    };

    state.messages.push(newMsg);
  },
  DELETE_SNACKBAR_MESSAGE(state, id) {
    const index = state.messages.findIndex((msg) => msg.id === id);

    if (index >= 0) {
      state.messages.splice(index, 1);
    }
  }
};

export const state = () => ({
  messages: []
});
