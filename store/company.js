export const actions = {
  async getCompany({ commit }) {
    try {
      const { data } = await this.$axios.get('/companies/me');
      await commit('SET_COMPANY', data);
    } catch ({ response }) {
      const msg = {
        status: response.data.statusCode,
        info: response.data.message,
        error: response.data.error
      };
      commit('notifications/ADD_SNACKBAR_MESSAGE', msg, { root: true });
    }
  }
};
export const mutations = {
  SET_COMPANY(state, company) {
    state.company = company;
  }
};

export const getters = {};

export const state = () => ({
  company: {}
});
