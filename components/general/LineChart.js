import { Line, mixins } from 'vue-chartjs';

export default {
  extends: Line,
  props: ['options'],
  mixins: [mixins.reactiveProp],
  mounted() {
    this.renderChart(this.chartData, this.options);
  },
  watch: {
    'chartData.labels': {
      handler(newValue, oldValue) {
        // console.log(newValue);
        // console.log('old', newValue, oldValue);
        // const oldData = [...oldValue.datasets[0].data];
        // const newData = [...newValue.datasets[0].data];
        // const isObjEqual = isEqual(newData.sort(), oldData.sort());
        //
        // if (!isObjEqual) {
        this.$data._chart.update();
        // }
      }
      // deep: true
    }
  }
};
