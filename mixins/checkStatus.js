export default {
  methods: {
    getObjectiveStatus(objectiveStatus) {
      switch (objectiveStatus) {
        case 'On Track':
          return {
            image: 'onTrack.svg',
            class: 'dashboard-objective--on-track'
          };
        case 'At Risk':
          return {
            image: 'atRisk.svg',
            class: 'dashboard-objective--at-risk'
          };
        case 'Off Track':
          return {
            image: 'offTrack.svg',
            class: 'dashboard-objective--off-track'
          };
        case 'Completed':
          return {
            image: 'completed.svg',
            class: 'dashboard-objective--completed'
          };
        case 'Ahead of schedule':
          return {
            image: 'aheadOfSchedule.svg',
            class: 'dashboard-objective--ahead-of-schedule'
          };
        case 'Not started':
          return {
            image: 'notStarted.svg',
            class: 'dashboard-objective--not-started'
          };
        default:
          return {
            image: 'notStarted.svg'
          };
      }
    }
  }
};
